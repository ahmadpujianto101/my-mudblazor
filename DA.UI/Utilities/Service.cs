using RestSharp;
using Newtonsoft.Json;
using System.Net.Http;
using System.Threading.Tasks;
using System.Collections.Generic;
using System;
using System.IO;

namespace DA.UI.Utilities
{
    public class Service
    {
        public static async Task<RestResponse> GETDownloadAsync(string baseURL, string resource, string? token = null)
        {
            var options = new RestClientOptions(baseURL)
            {
                MaxTimeout = -1,
            };
            var client = new RestClient(options);
            var request = new RestRequest(resource, Method.Get);

            if (token != null)
            {
                request.AddHeader("Authorization", $"Bearer {token}");
            }

            request.AddHeader("Content-Type", "application/json");

            var response = await client.ExecuteAsync(request) as RestResponse;

            return response;
        }
        public static async Task<byte[]> DownloadFileAsync(string baseURL, string resource, string? token = null)
        {
            var response = await GETDownloadAsync(baseURL, resource, token);

            if (response.IsSuccessful)
            {
                return response.RawBytes;
            }
            else
            {
                throw new Exception($"Error downloading file: {response.StatusCode}");
            }
        }
        public static RestResponse POST(string baseURL, string resource, object param)
        {
            var options = new RestClientOptions(baseURL)
            {
                MaxTimeout = -1,
            };
            var client = new RestClient(options);
            var request = new RestRequest(resource, Method.Post);
            request.AddHeader("Content-Type", "application/json");
            var body = JsonConvert.SerializeObject(param);
            request.AddStringBody(body, DataFormat.Json);
            RestResponse response = client.Execute(request);
            return response;
        }

        public static RestResponse POST(string baseURL, string resource, object param, List<ServiceHeaderModel> headers)
        {
            var options = new RestClientOptions(baseURL)
            {
                MaxTimeout = -1,
            };
            var client = new RestClient(options);
            var request = new RestRequest(resource, Method.Post);

            if (headers != null && headers.Count > 0)
            {
                foreach (var x in headers)
                {
                    request.AddHeader(x.Name, x.Value);
                    // request.AddHeader("Content-Type", "application/json");
                }
            }
            var body = JsonConvert.SerializeObject(param);
            request.AddStringBody(body, DataFormat.Json);
            RestResponse response = client.Execute(request);
            return response;
        }

        public static RestResponse GET(string baseURL, string resource, object? param = null, string? token = null)
        {
            var options = new RestClientOptions(baseURL)
            {
                MaxTimeout = -1,
            };
            var client = new RestClient(options);
            var request = new RestRequest(resource, Method.Get);

            if (token != null)
            {
                request.AddHeader("Authorization", $"Bearer {token}");
            }

            request.AddHeader("Content-Type", "application/json");

            RestResponse response = client.Execute(request);

            return response;
        }

        public static RestResponse PUT(string baseURL, string resource, object param, string? token = null)
        {
            var options = new RestClientOptions(baseURL)
            {
                MaxTimeout = -1,
            };
            var client = new RestClient(options);
            var request = new RestRequest(resource, Method.Put);
            request.AddHeader("Content-Type", "application/json");

            if (token != null)
            {
                request.AddHeader("Authorization", $"Bearer {token}");
            }

            var body = JsonConvert.SerializeObject(param);
            request.AddStringBody(body, DataFormat.Json);
            RestResponse response = client.Execute(request);
            return response;
        }

        public static RestResponse DELETE(string baseURL, string resource, string? token = null)
        {
            var options = new RestClientOptions(baseURL)
            {
                MaxTimeout = -1,
            };
            var client = new RestClient(options);
            var request = new RestRequest(resource, Method.Delete);
            request.AddHeader("Content-Type", "application/json");

            if (token != null)
            {
                request.AddHeader("Authorization", $"Bearer {token}");
            }

            RestResponse response = client.Execute(request);
            return response;
        }

        public static RestResponse POST_CLIENT(string baseURL, string resource, object param, string? token = null)
        {
            var options = new RestClientOptions(baseURL)
            {
                MaxTimeout = -1,
            };
            var client = new RestClient(options);
            var request = new RestRequest(resource, Method.Post);
            request.AddHeader("Content-Type", "application/json");

            if (token != null)
            {
                request.AddHeader("Authorization", $"Bearer {token}");
            }

            var body = JsonConvert.SerializeObject(param);
            request.AddStringBody(body, DataFormat.Json);
            RestResponse response = client.Execute(request);
            return response;
        }
    }

    public class ServiceHeaderModel
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}