

namespace DA.UI.Utilities
{

    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class BasicTable:Attribute
    {
        public string ColumnName {get; set;}
        public bool IsShow {get; set;} = true;
        public TypeColumn Type {get; set;} = TypeColumn.Data;
        public string ClassName {get; set;} = string.Empty;
        public string ContentHTML {get; set;} = string.Empty;
    }

    public enum TypeColumn
    {
        Data = 1,
        Link = 2,
        Button = 3
    }
}