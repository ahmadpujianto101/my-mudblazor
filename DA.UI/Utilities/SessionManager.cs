using Microsoft.JSInterop;

namespace DA.UI.Utilities
{
    public class SessionManager
    {
        private const string TokenKey = "token";

        public static async Task SetToken(IJSRuntime jSRuntime, string token)
        {
            await jSRuntime.InvokeVoidAsync("localStorage.setItem", TokenKey, token);
        }
        public static async Task SetUsername(IJSRuntime jSRuntime, string token)
        {
            await jSRuntime.InvokeVoidAsync("localStorage.setItem", "userName", token);
        }
        public static async Task SetUserId(IJSRuntime jSRuntime, int id)
        {
            await jSRuntime.InvokeVoidAsync("localStorage.setItem", "userId", id);
        }
        public static async Task SetName(IJSRuntime jSRuntime, string id)
        {
            await jSRuntime.InvokeVoidAsync("localStorage.setItem", "displayName", id);
        }
        public static async Task SetRole(IJSRuntime jSRuntime, string id)
        {
            await jSRuntime.InvokeVoidAsync("localStorage.setItem", "role", id);
        }
        public static async Task SetDepartmentId(IJSRuntime jSRuntime, string id)
        {
            await jSRuntime.InvokeVoidAsync("localStorage.setItem", "department_id", id);
        }
        public static async Task<string> GetToken(IJSRuntime jsRuntime)
        {
            return await jsRuntime.InvokeAsync<string>("localStorage.getItem", TokenKey);
        }
        public static async Task<string> GetName(IJSRuntime jsRuntime)
        {
            return await jsRuntime.InvokeAsync<string>("localStorage.getItem", "displayName");
        }
        public static async Task<string> GetUserId(IJSRuntime jsRuntime)
        {
            return await jsRuntime.InvokeAsync<string>("localStorage.getItem", "userId");
        }
        public static async Task<string> GetUserRole(IJSRuntime jsRuntime)
        {
            return await jsRuntime.InvokeAsync<string>("localStorage.getItem", "role");
        }
        public static async Task RemoveToken(IJSRuntime jsRuntime)
        {
            await jsRuntime.InvokeVoidAsync("localStorage.removeItem", TokenKey);
        }
    }
}