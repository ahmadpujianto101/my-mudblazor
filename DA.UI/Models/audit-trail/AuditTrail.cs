namespace DA.UI.Models.audit_trail.AuditTrail
{
    public class ResponseAuditTrail
    {
        public int Index { get; set; }
        public string id { get; set; }
        public string? method { get; set; }
        public string? action { get; set; }
        public string? description { get; set; }
        public DateTime? Created_At { get; set; }
        public string? Created_By { get; set; }
        public bool? deleted { get; set; }
    }
}