namespace DA.UI.Models
{
    public class LoginInputModel
    {
        public string? UserName { get; set; }
        public string? Password { get; set; }
        public int employee_id { get; set; }
        public string? displayName { get; set; }
        public string? role { get; set; }
        public string? department_id { get; set; }
    }

    public class LoginResultModel
    {
        public LoginInputModel? UserInfo { get; set; }
        public string? AccessToken { get; set; }
        public string? RefreshToken { get; set; }
    }
}