using System.ComponentModel.DataAnnotations;

namespace DA.UI.Models.Shared.Main
{
    public class ResponseCountry
    {
        public string CountryId { get; set; } = string.Empty;
        public string CountryName { get; set; } = string.Empty;
        public string TwoLetterIsoCode { get; set; } = string.Empty;
        public string ThreeLetterIsoCode { get; set; } = string.Empty;
        public bool AllowsBilling { get; set; } = true;
        public bool AllowsShipping { get; set; } = true;
        public string? NumericIsoCode { get; set; } = string.Empty;
        public bool SubjectToVat { get; set; } = true;
        public bool Published { get; set; } = true;
        public int DisplayOrder { get; set; }
        public bool LimitedToStores { get; set; } = true;
        public int TotalPopulation { get; set; }
        // public bool? deleted { get; set; }
    }

    public class ResponseProduct
    {
        public int ProductId { get; set; }

        [Required(ErrorMessage = "Field Product Name Is Required")]
        public string Name { get; set; } = string.Empty;
        public string MetaKeywords { get; set; } = string.Empty;
        public string MetaTitle { get; set; } = string.Empty;
        public string Sku { get; set; } = string.Empty;
        public string ManufacturerPartNumber { get; set; } = string.Empty;
        public string ShortDescription { get; set; } = string.Empty;
        public string FullDescription { get; set; } = string.Empty;
        public string AdminComment { get; set; } = string.Empty;
        public string ShowOnHomepage { get; set; } = string.Empty;
        public string MetaDescription { get; set; } = string.Empty;
        public double Price { get; set; }
        public double OldPrice { get; set; }
        public double ProductCost { get; set; }
        public double Weight { get; set; }
        public double Length { get; set; }
        public double Width { get; set; }
        public double Height { get; set; }
        public DateTime? AvailableStartDateTimeUtc { get; set; }
        public DateTime? AvailableEndDateTimeUtc { get; set; }
        public int DisplayOrder { get; set; }
        public bool Published { get; set; }
        public bool Deleted { get; set; }
        public DateTime? CreatedOnUtc { get; set; }
        public DateTime? UpdatedOnUtc { get; set; }
        public int StockRemaining { get; set; }
    }

    public class ResponseProductQuantity
    {
        public int ProductQuantityId { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; } = string.Empty;
        public DateTime RestockDate { get; set; }
        public int StockAdded { get; set; }
        public int RemainingStock { get; set; }
        public int EmployeeId { get; set; }
        public string Remarks { get; set; } = string.Empty;
    }

    public class ResponseOrder
    {
        public int OrderId { get; set; }
        public int CustomerId { get; set; }
        public string CustomerName { get; set; } = string.Empty;
        public int ProductId { get; set; }
        public string ProductName { get; set; } = string.Empty;
        public int OrderStatusId { get; set; }
        public int ShippingStatusId { get; set; }
        public int PaymentStatusId { get; set; }
        public string PaymentMethodSystemName { get; set; } = string.Empty;
        public double OrderSubtotal { get; set; }
        public double OrderDiscount { get; set; }
        public double OrderTotal { get; set; }
        public bool Deleted { get; set; }
        public DateTime CreatedOnUtc { get; set; }
        public int RedeemedRewardPointsEntryId { get; set; } 
        public string Sku { get; set; } = string.Empty;
    }

    public class ResponseStateProvince
    {
        public int StateProvinceId { get; set; }
        public string Name { get; set; } = string.Empty;
        public string Abbreviation { get; set; } = string.Empty;
        public int CountryId { get; set; }
        public string CountryName { get; set; } = string.Empty;
        public bool Published { get; set; }
        public int DisplayOrder { get; set; }
        public int TotalPopulation { get; set; }
    }
}
