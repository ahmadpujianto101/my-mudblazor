using Newtonsoft.Json;

namespace DA.UI.Models.Currency
{
    public class Currencys
    {
        public int Index { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }

        [JsonProperty("currency_to_idr")]
        public int CurrencyToIdr { get; set; }
        public bool Deleted { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedAt { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime UpdatedAt { get; set; }
    }

    public class ApprovalType
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Deleted { get; set; }
    }
}