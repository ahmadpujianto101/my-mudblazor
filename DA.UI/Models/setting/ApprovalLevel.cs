using Newtonsoft.Json;

namespace DA.UI.Models.ApprovalLevel
{
    public class ApprovalLevels
    {
        public int Index { get; set; }
        public int Id { get; set; }
        public string No { get; set; }

        [JsonProperty("type_id")]
        public int TypeId { get; set; }
        public string Type { get; set; }
        public int Level { get; set; }

        [JsonProperty("min_amount")]
        public decimal MinAmount { get; set; }

        [JsonProperty("currency_id")]
        public int CurrencyId { get; set; }
        public string Currency { get; set; }
        public bool Deleted { get; set; }
    }
}