using Newtonsoft.Json;

namespace DA.UI.Models.ApprovalUser
{
    public class ApprovalUsers
    {
        public int Index { get; set; }
        public int Id { get; set; }
        public string No { get; set; }

        [JsonProperty("type_id")]
        public int TypeId { get; set; }
        public string Type { get; set; }
        public int Level { get; set; }

        [JsonProperty("departement_id")]
        public string DepartmentId { get; set; }

        [JsonProperty("min_amount")]
        public decimal MinAmount { get; set; }

        [JsonProperty("currency_id")]
        public int CurrencyId { get; set; }

        [JsonProperty("is_default")]
        public bool IsDefault { get; set; }
        public string Employee { get; set; }

        [JsonProperty("employee_id")]
        public string EmployeeId { get; set; }
        public string Currency { get; set; }
        public bool Deleted { get; set; }
    }
}