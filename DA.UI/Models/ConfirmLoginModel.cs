namespace DA.UI.Models
{

    public class ConfirmLoginInputModel
    {
        public string? Password { get; set; }
        public string? NewPassword { get; set; }
    }
}