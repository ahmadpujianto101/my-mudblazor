using Newtonsoft.Json;

namespace DA.UI.Models.Shared.AttachmentType
{
    public class AttachmentTypes
{
    [JsonProperty("id")]
    public int Id { get; set; }
    [JsonProperty("name")]
    public string Name { get; set; }
    [JsonProperty("deleted")]
    public bool Deleted { get; set; }
    public string Created_By { get; set; }
    public DateTime Created_At { get; set; }
    public string Update_By { get; set; }
    public bool? Is_Active { get; set; }
}

public class AttachmentTypeResponse
{
    public string Message { get; set; }
    public List<AttachmentTypes> Data { get; set; }
    public int TotalPage { get; set; }
    public int TotalCount { get; set; }
    public int CurrentPage { get; set; }
    public int ShowPerPage { get; set; }
}
}