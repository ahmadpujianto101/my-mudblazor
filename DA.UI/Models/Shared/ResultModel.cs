namespace DA.UI.Models
{
    public class ResultModel<T>
    {
        public string Message {get; set;} = string.Empty;
        public T? Data {get; set;} 
    }
}