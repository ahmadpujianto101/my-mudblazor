using Newtonsoft.Json;

namespace DA.UI.Models.DashboardTableUserProfile
{
    public class DashboardTableUserProfile
    {
        public int Index { get; set; }
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("department_id")]
        public string DepartmentId { get; set; }

        [JsonProperty("department")]
        public string Department { get; set; }

        [JsonProperty("sub_department_id")]
        public string SubDepartmentId { get; set; }

        [JsonProperty("sub_department")]
        public string SubDepartment { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("level_access_id")]
        public string LevelAccessId { get; set; }

        [JsonProperty("level_access")]
        public string LevelAccess { get; set; }

        [JsonProperty("status")]
        public int Status { get; set; }

        [JsonProperty("status_name")]
        public string StatusName { get; set; }
    }
}