namespace DA.UI.Models.DashboardTable
{
    public class DashboardTables
    {
        public int PageSize { get; set; }
        public string? UserID { get; set; }
        public string? Id { get; set; }
        public string? Title { get; set; }
        public string? Body { get; set; }
    }
}