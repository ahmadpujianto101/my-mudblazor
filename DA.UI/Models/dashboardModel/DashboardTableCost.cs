using Newtonsoft.Json;

namespace DA.UI.Models.DashboardTableCost
{
    public class DashboardTableCosts
    {
        public int Index { get; set; }
        [JsonProperty("cost_centerId")]
        public string CostCenterId { get; set; }

        [JsonProperty("cost_centerName")]
        public string CostCenterName { get; set; }

        [JsonProperty("cost_centerOwnerId")]
        public int CostCenterOwnerId { get; set; }

        [JsonProperty("cost_centerOwnerName")]
        public string CostCenterOwnerName { get; set; }

        [JsonProperty("status")]
        public int Status { get; set; }

        [JsonProperty("created_By")]
        public string CreatedBy { get; set; }

        [JsonProperty("created_At")]
        public DateTime CreatedAt { get; set; }

        [JsonProperty("update_By")]
        public string UpdateBy { get; set; }

        [JsonProperty("categoryCostCenter")]
        public string CategoryCostCenter { get; set; }
        public int CategoryCostCenter_id { get; set; }

        [JsonProperty("update_At")]
        public DateTime UpdateAt { get; set; }
        public int PageSize { get; set; }
        public bool? deleted { get; set; } = false;
    }

    public class CategoryCostCenter
    {
        public int Index { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Deleted { get; set; }
    }
}