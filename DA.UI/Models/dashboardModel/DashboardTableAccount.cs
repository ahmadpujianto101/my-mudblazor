using Newtonsoft.Json;

namespace DA.UI.Models.DashboardTableAccount
{
    public class DashboardTableAccounts
    {
        public int Index { get; set; }
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("is_active")]
        public bool IsActive { get; set; }

        [JsonProperty("deleted")]
        public bool Deleted { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("created_By")]
        public string CreatedBy { get; set; }

        [JsonProperty("categoryAccount_id")]
        public string CategoryAccount { get; set; }

        [JsonProperty("created_At")]
        public DateTime CreatedAt { get; set; }

        [JsonProperty("update_By")]
        public string UpdateBy { get; set; }

        [JsonProperty("update_At")]
        public DateTime? UpdateAt { get; set; }

        public string? CategoryParent { get; set; }
        public string? SubCategoryParent { get; set; }
        public string? Scanned { get; set; }
    }
}