namespace DA.UI.Models
{
    public class TestTableModel
    {
        public int Page { get; set; }
        public int PageSize { get; set; }
        public string? UserID { get; set; }
        public string? Id { get; set; }
        public string? Title { get; set; }
        public string? Body { get; set; }
    }
}