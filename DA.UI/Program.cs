using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;
//using DA.UI.Data;
using MudBlazor.Services;
using Sentry;
using Microsoft.AspNetCore.Diagnostics;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddRazorPages();
builder.Services.AddServerSideBlazor();
//builder.Services.AddSingleton<WeatherForecastService>();
builder.Configuration.AddJsonFile("appsettings.json");
builder.Services.AddMudServices(config =>
{
    config.SnackbarConfiguration.PositionClass = "bottom-left";
    config.SnackbarConfiguration.VisibleStateDuration = 2000;

    config.SnackbarConfiguration.PreventDuplicates = false;
    config.SnackbarConfiguration.NewestOnTop = false;
    config.SnackbarConfiguration.ShowCloseIcon = true;
    config.SnackbarConfiguration.HideTransitionDuration = 500;
    config.SnackbarConfiguration.ShowTransitionDuration = 500;
});
builder.WebHost.UseSentry();

var app = builder.Build();

app.UseSentryTracing();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler(errorApp =>
    {
        errorApp.Run(async context =>
        {
            var exceptionHandlerPathFeature = context.Features.Get<IExceptionHandlerPathFeature>();
            var exception = exceptionHandlerPathFeature?.Error;
            SentrySdk.CaptureException(exception);

            // Handle atau tampilkan halaman kesalahan sesuai kebutuhan Anda

            context.Response.StatusCode = 500; // Atur kode status sesuai kebutuhan Anda
        });
    });

    // app.UseExceptionHandler("/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();

app.UseStaticFiles();

app.UseRouting();

app.MapBlazorHub();
app.MapFallbackToPage("/_Host");

// Inisialisasi Sentry
SentrySdk.Init(options =>
{
    options.Dsn = "https://2c5660ea97c266476fbe0058c894cfdf@o4505645842038784.ingest.sentry.io/4505645916815360";
    // Konfigurasikan opsi lainnya jika diperlukan
    options.Debug = true;
});

app.Run();
