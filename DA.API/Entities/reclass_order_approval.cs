using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace DA.Entities
{ 
	public class reclass_order_approval
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int id { get; set; }


		public int? reclass_approval_type { get; set; }


		public int? reclass_order_id { get; set; }


		public int? employee_id { get; set; }


		public int? status { get; set; }


		public string? remark { get; set; }


		public string? Created_By { get; set; }


		public DateTime? Created_At { get; set; }


		public string? Update_By { get; set; }


		public DateTime? Update_At { get; set; }


		public bool? deleted { get; set; }


	} 
}