using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace DA.Entities
{ 
	public class sub_department
	{
		[Key]
		public string id { get; set; }


		public string? name { get; set; }


		public string? department_id { get; set; }


		public string? Created_By { get; set; }


		public DateTime? Created_At { get; set; }


		public string? Update_By { get; set; }


		public DateTime? Update_At { get; set; }


		public bool? Deleted { get; set; }


		public bool? is_active { get; set; }


	} 
}