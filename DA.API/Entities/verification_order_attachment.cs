﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace DA.Entities
{
    public class verification_order_attachment
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }


        public int purchase_order_id { get; set; }


        public int type_id { get; set; }


        public string? attachment { get; set; }


        public bool? deleted { get; set; }


        public string? Created_By { get; set; }


        public DateTime? Created_At { get; set; }


        public string? Update_By { get; set; }


        public DateTime? Update_At { get; set; }
    }
}
