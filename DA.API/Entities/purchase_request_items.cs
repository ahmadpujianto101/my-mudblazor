using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace DA.Entities
{ 
	public class purchase_request_items
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int id { get; set; }


		public int? purchase_request_id { get; set; }


		public string? description { get; set; }


		public int? qty { get; set; }


		public int? uom_id { get; set; }


		public string? cost_center_id { get; set; }


		public string? account_code_id { get; set; }


		public int? vat { get; set; }


		public int? price { get; set; }


		public string? attachment { get; set; }


		public bool? deleted { get; set; }


		public string? Created_By { get; set; }


		public DateTime? Created_At { get; set; }


		public string? Update_By { get; set; }


		public DateTime? Update_At { get; set; }


		public bool? is_active { get; set; }


	} 
}