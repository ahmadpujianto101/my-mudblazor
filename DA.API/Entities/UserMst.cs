using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace DA.Entities
{ 
	public class UserMst
	{
		public string UserName { get; set; }


		public string? DisplayName { get; set; }


		public string Password { get; set; }


		public string? Remark { get; set; }


		public bool? Deleted { get; set; }


		public string? Created_By { get; set; }


		public DateTime? Created_At { get; set; }


		public string? Update_By { get; set; }


		public DateTime? Update_At { get; set; }


		public bool? is_active { get; set; }


	} 
}