using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace DA.Entities
{ 
	public class supplier
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int id { get; set; }


		[DatabaseGenerated(DatabaseGeneratedOption.Computed)]
		public string? code { get; set; }


		public string? name { get; set; }


		public string? address { get; set; }


		public string? telp { get; set; }


		public string? mobile { get; set; }


		public string? npwp { get; set; }


		public string? pic_name { get; set; }


		public string? pic_email { get; set; }


		public string? pic_nik { get; set; }


		public string? tdp { get; set; }


		public string? siup { get; set; }


		public string? nib { get; set; }


		public string? srm { get; set; }


		public bool? is_active { get; set; }


		public int? type_id { get; set; }


		public int? category_id { get; set; }


		public int? province_id { get; set; }


		public string? Created_By { get; set; }


		public DateTime? Created_At { get; set; }


		public string? Update_By { get; set; }


		public DateTime? Update_At { get; set; }


		public bool? deleted { get; set; }


	} 
}