using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace DA.Entities
{ 
	public class purchase_request_review
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int id { get; set; }


		public int? purchase_request_id { get; set; }


		public int? vendor_type { get; set; }


		public int? vendor_tiering { get; set; }


		public int? transaction_level { get; set; }


		public string? authorized_signatured { get; set; }


		public string? docs_review { get; set; }


		public string? due_dilligence { get; set; }


		public string? legal_docs { get; set; }


		public string note { get; set; }


		public bool? deleted { get; set; }


		public string? Created_By { get; set; }


		public DateTime? Created_At { get; set; }


		public string? Update_By { get; set; }


		public DateTime? Update_At { get; set; }


		public bool? is_active { get; set; }


	} 
}