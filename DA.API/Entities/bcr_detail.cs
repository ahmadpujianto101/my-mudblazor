using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace DA.Entities
{ 
	public class bcr_detail
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int id { get; set; }


		public int? bcr_id { get; set; }


		public string? account_code_id { get; set; }


		public string? account_name { get; set; }


		public DateTime? date { get; set; }


		public string? cost_center_id { get; set; }


		public string? currency { get; set; }


		public decimal? original_amount { get; set; }


		public decimal? amount_in_idr { get; set; }


		public string description { get; set; }


		public string? seg_cd { get; set; }


		public string? iss_cd { get; set; }


		public string? destrb_ch { get; set; }


		public string? cost_center_name { get; set; }


		public DateTime? period { get; set; }


		public string? journal_type { get; set; }


		public string? category { get; set; }


		public string? Created_By { get; set; }


		public DateTime? Created_At { get; set; }


		public string? Update_By { get; set; }


		public DateTime? Update_At { get; set; }


		public bool? is_active { get; set; }


		public bool? deleted { get; set; }


	} 
}