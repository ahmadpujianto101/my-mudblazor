using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace DA.Entities
{ 
	public class EmailLogger
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public long id { get; set; }


		public string? reciever { get; set; }


		public string? valid_reciever { get; set; }


		public string? cc { get; set; }


		public string? bcc { get; set; }


		public string? subject { get; set; }


		public string bodyMessage { get; set; }


		public bool? IsBodyHtml { get; set; }


		public string? PathAttachment { get; set; }


		public string? TransactionType { get; set; }


		public string? EmailStatus { get; set; }


		public string? StatusMsg { get; set; }


        public bool? deleted { get; set; }


        public string? Created_By { get; set; }


        public DateTime? Created_At { get; set; }


        public string? Update_By { get; set; }


        public DateTime? Update_At { get; set; }


        public bool? is_active { get; set; }

    } 
}