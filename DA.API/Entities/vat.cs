using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace DA.Entities
{ 
	public class Vat
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int id { get; set; }
		public string vat_name { get; set; }
		public int? vat_value { get; set; }
		public bool? deleted { get; set; }
		public bool? is_active { get; set; }
		public string? Created_By { get; set; }
		public DateTime? Created_At { get; set; }
		public string? Update_By { get; set; }
		public DateTime? Update_At { get; set; }
	} 
}