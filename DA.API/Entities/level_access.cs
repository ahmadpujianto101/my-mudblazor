using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace DA.Entities
{ 
	public class level_access
	{
		[Key]
		public string id { get; set; }


		public string? name { get; set; }


		public string? Created_By { get; set; }


		public DateTime? Created_At { get; set; }


		public string? Update_By { get; set; }


		public DateTime? Update_At { get; set; }


		public bool? is_active { get; set; }


		public bool? deleted { get; set; }


	} 
}