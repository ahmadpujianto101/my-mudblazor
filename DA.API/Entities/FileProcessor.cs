using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace DA.Entities
{ 
	public class FileProcessor
	{
		[Key]
		public Guid ID { get; set; }


		public string? FileName { get; set; }


		public string? ProcessType { get; set; }


		public string? Status { get; set; }


		public string? FilePath { get; set; }


		public DateTime? ImportedOn { get; set; }


		public DateTime? StartedOn { get; set; }


		public DateTime? EndOn { get; set; }


		public string? ImportedBy { get; set; }


		public bool? deleted { get; set; }

		public string Message { get; set; }


	} 
}