﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace DA.Entities
{
    public class delivery_order
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }


        public string? po_number { get; set; }


        public string? qa_number { get; set; }


        public string? pr_number { get; set; }


        public string? do_number { get; set; }


        public int? type { get; set; }


        public int? purpose { get; set; }


        public int? channel_budget_id { get; set; }


        public int? currency_id { get; set; }


        public int? shipment_id { get; set; }


        public int? supplier_id { get; set; }


        public string? note { get; set; }


        public int? status { get; set; }


        public bool? deleted { get; set; }


        public string? Created_By { get; set; }


        public DateTime? Created_At { get; set; }


        public string? Update_By { get; set; }


        public DateTime? Update_At { get; set; }
    }
}
