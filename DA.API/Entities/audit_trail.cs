﻿using System.ComponentModel.DataAnnotations;


namespace DA.Entities
{
    public class audit_trail
    {
        [Key]
        public string id { get; set; }

        public string? method { get; set; }

        public string? action { get; set; }

        public string? description { get; set; }

        public DateTime? Created_At { get; set; }

        public string? Created_By { get; set; }

        public bool? deleted { get; set; }
    }
}
