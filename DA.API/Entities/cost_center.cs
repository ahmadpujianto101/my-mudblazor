using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace DA.Entities
{ 
	public class cost_center
	{
		[Key]
		public string id { get; set; }


		public string? name { get; set; }


		public int? owner_id { get; set; }


		public bool? is_active { get; set; }


		public DateTime? Created_At { get; set; }


		public string? Created_By { get; set; }


		public string? Update_By { get; set; }


		public DateTime? Update_At { get; set; }


		public int? CategoryCostCenter_id { get; set; }


		public bool? deleted { get; set; }


	} 
}