using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace DA.Entities
{ 
	public class bcr
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int id { get; set; }


		public DateTime? period { get; set; }


		public string? file { get; set; }


		public bool? final { get; set; }


		public string? actual_report { get; set; }


		public string? Created_By { get; set; }


		public DateTime? Created_At { get; set; }


		public string? Update_By { get; set; }


		public DateTime? Update_At { get; set; }


		public bool? is_active { get; set; }


		public bool? deleted { get; set; }


	} 
}