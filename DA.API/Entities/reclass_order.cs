using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace DA.Entities
{ 
	public class reclass_order
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int id { get; set; }


		public DateTime? period { get; set; }


		public string? department_id { get; set; }


		public string? cost_center_id { get; set; }


		public string? from_account_code_id { get; set; }


		public string? to_account_code_id { get; set; }


		public string? reason { get; set; }


		public int? amount { get; set; }


		public int? status { get; set; }


		public int? reclass_approval_type { get; set; }


		public string? Created_By { get; set; }


		public DateTime? Created_At { get; set; }


		public string? Update_By { get; set; }


		public DateTime? Update_At { get; set; }


		public bool? deleted { get; set; }


	} 
}