using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace DA.Entities
{ 
	public class budget_master_detail
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int id { get; set; }


		public int? budget_master_id { get; set; }


		public int? month { get; set; }


		public string? account_code_id { get; set; }


		public decimal? amount { get; set; }


		public string? curr { get; set; }


		public decimal? OriginalAmount { get; set; }


		public decimal? AmountInIDR { get; set; }


		public string? Description { get; set; }


		public string? SEG_CD { get; set; }


		public string? ISS_CD { get; set; }


		public string? DSTRB_CH { get; set; }


		public string? CostctrName { get; set; }


		public string? PERIOD { get; set; }


		public string? JournalType { get; set; }


		public string? CostctrName_Val { get; set; }


		public string? Created_By { get; set; }


		public DateTime? Created_At { get; set; }


		public string? Update_By { get; set; }


		public DateTime? Update_At { get; set; }


		public bool? Deleted { get; set; }


		public bool? is_active { get; set; }


	} 
}