using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace DA.Entities
{ 
	public class employee
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int id { get; set; }


		public string? code { get; set; }


		public string? name { get; set; }


		public string? department_id { get; set; }


		public string? sub_department_id { get; set; }


		public string? email { get; set; }


		public string? level_access_id { get; set; }


		public bool? reviewer { get; set; }


		public bool? verification { get; set; }


		public int? status { get; set; }


		public string? Created_By { get; set; }


		public DateTime? Created_At { get; set; }


		public string? Update_By { get; set; }


		public DateTime? Update_At { get; set; }


		public bool? deleted { get; set; }


	} 
}