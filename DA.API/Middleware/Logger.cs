using System.Text;


namespace DA.Middleware
{
    public class LoggingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<LoggingMiddleware> _logger;

        public LoggingMiddleware(RequestDelegate next, ILogger<LoggingMiddleware> logger)
        {
            _next = next;
            _logger = logger;
        }

        public async Task Invoke(HttpContext context)
        {
            // Membaca body permintaan
            string requestBody = await ReadRequestBodyAsync(context.Request);

            // Mencatat informasi endpoint dan body permintaan
            var messageRequest = $@"Request: {context.Request.Method} {context.Request.Path}
            Body: {requestBody}";
            _logger.LogInformation(messageRequest);

            // Menyimpan respons untuk logging
            var originalBodyStream = context.Response.Body;
            using (var responseBody = new MemoryStream())
            {
                context.Response.Body = responseBody;

                // Memanggil middleware selanjutnya
                await _next(context);

                // Membaca respons
                string responseBodyText = await ReadResponseBodyAsync(context.Response);

                // Mencatat informasi respons
                var messageResponse = $@"Response: {context.Request.Method} {context.Request.Path} 
                StatusCode: {context.Response.StatusCode}
                Body: {responseBodyText}";

                if(context.Response.StatusCode == StatusCodes.Status200OK)
                    _logger.LogInformation(messageResponse);
                else
                    _logger.LogError(messageResponse);

                // Mengembalikan respons ke stream asli
                await responseBody.CopyToAsync(originalBodyStream);
            }
        }

        private async Task<string> ReadRequestBodyAsync(HttpRequest request)
        {
            request.EnableBuffering();

            using (var reader = new StreamReader(request.Body, Encoding.UTF8, true, 1024, true))
            {
                string body = await reader.ReadToEndAsync();
                request.Body.Seek(0, SeekOrigin.Begin);
                return body;
            }
        }

        private async Task<string> ReadResponseBodyAsync(HttpResponse response)
        {
            response.Body.Seek(0, SeekOrigin.Begin);
            string body = await new StreamReader(response.Body, Encoding.UTF8).ReadToEndAsync();
            response.Body.Seek(0, SeekOrigin.Begin);
            return body;
        }
    }
}