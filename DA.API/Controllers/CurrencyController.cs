﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using DA.Models;
using DA.Utilities;
using DA.API.Models;

namespace DA.API.Controllers
{
    [ApiController]
    [Route("api/currency")]
    public class CurrencyController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        public CurrencyController(IConfiguration configuration)
        {
            _configuration = configuration;
        }


        //[Authorize]
        //[HttpGet]
        //public ActionResult GetALL(int? page = null, int? rows = null, string? order = null)
        //{
        //    RequestPaging paging = null;
        //    if (page != null || rows != null || order != null)
        //        paging = new RequestPaging() { CurrentPage = page ?? 1, ShowPerPage = rows ?? 10, orderBy = order };
        //    var result = new ResultPaging<List<CurrencyViewModel>>();
        //    try
        //    {
        //        //result = Internals.Currency.GetAll(paging);
        //        return Ok(result);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.WriteToConsole(Logger.Level.Error, ex.Message);
        //        result.Data = null;
        //        result.Message = ex.Message;
        //        return BadRequest(result);
        //    }

        //}

        //[Authorize]
        //[HttpGet("{id}")]
        //public ActionResult GetData(int id)
        //{
        //    var result = new ResultModel<CurrencyViewModel>();
        //    try
        //    {
        //        result = Internals.Currency.Get(id);
        //        return Ok(result);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.WriteToConsole(Logger.Level.Error, ex.Message);
        //        result.Data = null;
        //        result.Message = ex.Message;
        //        return BadRequest(result);
        //    }
        //}

        //[Authorize]
        //[HttpPost]
        //public ActionResult AddData(CurrencyInputModel data)
        //{
        //    if (!ModelState.IsValid)
        //        return BadRequest(ModelState);

        //    string userName = User.Claims.FirstOrDefault(o => o.Type == "UserName").Value;
        //    var result = new ResultModel<bool>();

        //    try
        //    {
        //        result = Internals.Currency.Insert(data, userName);
        //        if (result.Data)
        //            return Ok(result);
        //        else
        //            return BadRequest(result);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.WriteToConsole(Logger.Level.Error, ex.Message);
        //        result.Data = false;
        //        result.Message = ex.Message;
        //        return BadRequest(result);
        //    }
        //}

        //[Authorize]
        //[HttpPost("edit/{id}")]
        //public ActionResult EditData(int id, [FromBody] CurrencyInputModel data)
        //{
        //    if (!ModelState.IsValid)
        //        return BadRequest(ModelState);

        //    string userName = User.Claims.FirstOrDefault(o => o.Type == "UserName").Value;
        //    var result = new ResultModel<bool>();
        //    try
        //    {
        //        result = Internals.Currency.Update(id, data, userName);
        //        if (result.Data)
        //            return Ok(result);
        //        else
        //            return BadRequest(result);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.WriteToConsole(Logger.Level.Error, ex.Message);
        //        result.Data = false;
        //        result.Message = ex.Message;
        //        return BadRequest(result);
        //    }
        //}

        //[Authorize]
        //[HttpPost("delete/{id}")]
        //public ActionResult DeleteData(int id)
        //{
        //    string userName = User.Claims.FirstOrDefault(o => o.Type == "UserName").Value;
        //    var result = new ResultModel<bool>();
        //    try
        //    {
        //        result = Internals.Currency.Delete(id, userName);
        //        if (result.Data)
        //            return Ok(result);
        //        else
        //            return BadRequest(result);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.WriteToConsole(Logger.Level.Error, ex.Message);
        //        result.Data = false;
        //        result.Message = ex.Message;
        //        return BadRequest(result);
        //    }
        //}

    }
}
