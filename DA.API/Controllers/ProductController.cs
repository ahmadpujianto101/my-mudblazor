using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using DA.Models;
using DA.Utilities;

namespace DA.API.Controllers
{
    [ApiController]
    [Route("api/product")]
    public class ProductController : ControllerBase
    {
        [AllowAnonymous]
        [HttpGet]
        public ActionResult Get(int? page = null, int? rows = null, string? order = null)
        {
            RequestPaging? paging = null;
            if (page != null || rows != null || order != null)
                paging = new RequestPaging() { CurrentPage = page ?? 1, ShowPerPage = rows ?? 10, orderBy = order };
            var result = new ResultPaging<List<ProductModel>>();
            try
            {
                result = Internals.Product.Get(paging);
                return Ok(result);
            }
            catch (Exception ex)
            {
                Logger.WriteToConsole(Logger.Level.Error, ex.Message);
                result.Data = null;
                result.Message = ex.Message;
                return BadRequest(result);
            }
        }


        //[Authorize]
        //[HttpPost]
        //public ActionResult Add(CostCenterInputModel data)
        //{
        //    if (!ModelState.IsValid)
        //        return BadRequest(ModelState);

        //    string userName = User.UserName();
        //    var result = new ResultModel<bool>();
        //    try
        //    {
        //        result = Internals.CostCenter.Insert(data, userName);
        //        return Ok(result);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.WriteToConsole(Logger.Level.Error, ex.Message);
        //        result.Data = false;
        //        result.Message = ex.Message;
        //        return BadRequest(result);
        //    }
        //}

        //[Authorize]
        //[HttpGet("{id}")]
        //public ActionResult GetById(string id)
        //{
        //    var result = new ResultModel<CostCenterViewModel>();
        //    try
        //    {
        //        if (string.IsNullOrEmpty(id))
        //            throw new Exception("Something went wrong when get data. Please Contact Administrator");

        //        result = Internals.CostCenter.Get(id);
        //        return Ok(result);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.WriteToConsole(Logger.Level.Error, ex.Message);
        //        result.Data = null;
        //        result.Message = ex.Message;
        //        return BadRequest(result);
        //    }
        //}

        
        //[Authorize]
        //[HttpPost("edit/{id}")]
        //public ActionResult Update(string id,[FromBody] CostCenterInputModel data)
        //{
        //    if (!ModelState.IsValid)
        //        return BadRequest(ModelState);


        //    string userName = User.UserName();
        //    var result = new ResultModel<bool>();

        //    try
        //    {
        //        result = Internals.CostCenter.Update(id, data, userName);
        //        return Ok(result);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.WriteToConsole(Logger.Level.Error, ex.Message);
        //        result.Data = false;
        //        result.Message = ex.Message;
        //        return BadRequest(result);
        //    }
        //}
        //[Authorize]
        //[HttpPost("{id}")]
        //public ActionResult Delete(string id)
        //{
        //    if (!ModelState.IsValid)
        //        return BadRequest(ModelState);


        //    string userName = User.UserName();
        //    var result = new ResultModel<bool>();

        //    try
        //    {
        //        result = Internals.CostCenter.Delete(id, userName);
        //        return Ok(result);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.WriteToConsole(Logger.Level.Error, ex.Message);
        //        result.Data = false;
        //        result.Message = ex.Message;
        //        return BadRequest(result);
        //    }
        //}
    }
}