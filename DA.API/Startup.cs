﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using DA.Middleware;
using Microsoft.OpenApi.Models;
using Microsoft.AspNetCore.Authentication.Negotiate;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Server.HttpSys;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace DA.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            // Configuration and Database Setup
            string connectionString = Configuration.GetConnectionString("SunLife");
            DA.Utilities.DataService.ConnectionString = connectionString;
            DA.Utilities.UpgradeDB.Execute();

            var MailConfig = Configuration.GetSection("Email");
            //if (MailConfig != null && MailConfig.GetChildren().Count() > 0)
            //{
            //    DA.Utilities.EmailService.sender = new DA.API.Models.EmailModel()
            //    {
            //        emailServer = MailConfig.GetValue<string>("Server") ?? "",
            //        emailPort = MailConfig.GetValue<int>("Port"),
            //        emailUsername = MailConfig.GetValue<string>("Username") ?? "",
            //        emailPassword = MailConfig.GetValue<string>("Password") ?? "",
            //        emailDisplay = MailConfig.GetValue<string>("emailDisplay") ?? "Sunlife",
            //        UseDefaultCredentials = MailConfig.GetValue<bool>("UseDefaultCredentials"),
            //    };
            //}

            // Controllers and Swagger Setup
            services.AddControllers();
            services.AddEndpointsApiExplorer();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "Sunfile_API",
                    Version = "v1"
                });
                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Name = "Authorization",
                    Type = SecuritySchemeType.ApiKey,
                    BearerFormat = "JWT",
                    In = ParameterLocation.Header,
                    Description = "Here Enter JWT Token with bearer format like bearer[space] token"
                });
                c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            }
                        },
                        new string[] { }
                    }
                });
            });

            // If using JWT Bearer, uncomment and configure the following lines
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        ValidIssuer = Configuration["Jwt:Issuer"],
                        ValidAudience = Configuration["Jwt:Audience"],
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Jwt:Key"]))
                    };
                });

            //services.AddAuthorization(options =>
            //{
            //    options.FallbackPolicy = options.DefaultPolicy;
            //});

            //// Authentication Setup
            //services.AddAuthentication(HttpSysDefaults.AuthenticationScheme);


            // CORS Setup
            var allOrigins = "allowOrigins";
            services.AddCors(opt => opt.AddPolicy(allOrigins,
                                            policy =>
                                            {
                                                policy.WithOrigins("*").AllowAnyMethod().AllowAnyHeader();
                                            }));

            services.AddHttpClient();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            // Development Environment Setup
            if (env.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            // Logging, HTTPS, and CORS Setup
            app.UseHttpLogging();
            app.UseHttpsRedirection();
            app.UseCors("allowOrigins");

            // Authentication and Authorization Setup
            app.UseAuthentication();
            app.UseAuthorization();

            // Routing Middleware Setup
            app.UseRouting();

            // Endpoint Mapping
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            // Logging Middleware Setup
            app.UseMiddleware<LoggingMiddleware>();
        }

    }
}
