using DA.Models;
using DA.Entities;
using DA.Utilities;

namespace DA.API.Internals
{
    public class StateProvince
    {

        public static ResultPaging<List<StateProvinceViewModel>> Get(RequestPaging? paging)
        {
            var res = new ResultPaging<List<StateProvinceViewModel>>();

            var sql = @"
                EXEC [SP_GetStateProvince]";

            if (paging != null)
            {
                if (string.IsNullOrEmpty(paging.orderBy))
                   // paging.orderBy = " Created_At desc ";

                res = DataService.FindListPaging<StateProvinceViewModel>(paging, sql);
            }
            else
            {
               // sql += " order by c.Created_At desc ";
                res.Data = DataService.FindList<StateProvinceViewModel>(sql);
                res.TotalCount = res.Data.Count;
            }
            
            if (res.Data != null && res.Data.Count > 0)
            {
                res.Message = "OK";
            }
            else
            {
                res.Message = "Data Not Found!";
            }
            return res;
        }
        //public static ResultModel<CostCenterViewModel> Get(string id)
        //{
        //    var res = new ResultModel<CostCenterViewModel>();
        //    var sql = @"
        //        select c.id[cost_centerId], 
        //        c.name [cost_centerName], 
        //        c.owner_id [cost_centerOwnerId], 
        //        e.name[cost_centerOwnerName],
        //        c.is_active [status], 
        //        c.CategoryCostCenter_id,
        //        cc.[name] CategoryCostCenter,
        //        c.Created_By, 
        //        c.Created_At, 
        //        c.Update_By, 
        //        c.Update_At,
        //        c.deleted
        //        from cost_center c
        //        inner join employee e on c.owner_id = e.id
        //        left join CategoryCostCenter cc on c.CategoryCostCenter_id = cc.id
        //        where c.id = @id
        //        and cc.[deleted] != 1";
        //    var data = DataService.Find<CostCenterViewModel>(sql, new { id });
        //    if (data != null)
        //    {
        //        res.Data = data;
        //        res.Message = "OK";
        //    }
        //    else
        //    {
        //        res.Message = "Data Not Found";
        //    }

        //    return res;
        //}
        //public static ResultModel<bool> Insert(CostCenterInputModel data, string userName)
        //{
        //    var result = new ResultModel<bool>();
        //    var dataValidation = Get(data.cost_centerId.ToString());
        //    if (dataValidation != null && dataValidation.Data != null)
        //    {
        //        result = Update(dataValidation.Data.cost_centerId, data, userName);
        //        //if (!(dataValidation.Data.deleted ?? false))
        //        //    throw new Exception($"Cost Center with ID {data.cost_centerId} already exist");
        //        //else
        //        //    result = Update(dataValidation.Data.cost_centerId, data, userName);
        //    }
        //    else
        //    {
        //        int page = 1; int rows = 10; string order = "";
        //        RequestPaging ? paging = null;
        //        if (page != null || rows != null || order != null)
        //            paging = new RequestPaging() { CurrentPage = page , ShowPerPage = rows, orderBy = order };

        //        var cat = Internals.Country.Get(paging); //data.CategoryCostCenter_id
        //        if (cat == null || cat.Data == null)
        //            throw new Exception($"Category Cost Center with ID {data.CategoryCostCenter_id} is Not Found");

        //        var sqlInsert = @"INSERT INTO cost_center (id, name, owner_id, CategoryCostCenter_id, is_active, created_at, created_by) 
        //                        values (@id, @name, @ownerId, @CategoryCostCenter_id, @isactive, @createdat, @createdby)";

        //        var exec = DataService.Execute(sqlInsert,
        //                                new
        //                                {
        //                                    id = data.cost_centerId,
        //                                    name = data.cost_centerName,
        //                                    ownerId = data.cost_centerOwnerId,
        //                                    data.CategoryCostCenter_id,
        //                                    isactive = data.status,
        //                                    createdat = DateTime.Now,
        //                                    createdby = userName
        //                                });
        //        if (exec <= 0)
        //            throw new Exception("Add Data Failed");

        //        result.Data = true;
        //        result.Message = "OK";
        //    }

        //    return result;
        //}
        //public static ResultModel<bool> Update(string id, CostCenterInputModel data, string userName)
        //{
        //    var result = new ResultModel<bool>();

        //    var isValid = Get(id);
        //    if (isValid == null || isValid.Data == null)
        //        throw new Exception($"Cost Center with ID {id} not exist");

        //    var sqlUpdate = @"UPDATE cost_center SET 
        //                        name = @name,
        //                        owner_id = @ownerId,
        //                        CategoryCostCenter_id = @CategoryCostCenter_id,
        //                        is_active = @isactive,
        //                        deleted = @deleted,
        //                        update_by = @updateBy,
        //                        update_at = @updateAt
        //                        WHERE id = @id";

        //    var exec = DataService.Execute(sqlUpdate,
        //               new
        //               {
        //                   id = id,
        //                   name = data.cost_centerName,
        //                   ownerId = data.cost_centerOwnerId,
        //                   data.CategoryCostCenter_id,
        //                   isactive = data.status,
        //                   data.deleted,
        //                   updateAt = DateTime.Now,
        //                   updateBy = userName                           
        //               });
        //    if (exec <= 0)
        //        throw new Exception("Update Data Failed");

        //    result.Data = true;
        //    result.Message = "OK";

        //    return result;
        //}

        //public static ResultModel<bool> Delete(string id, string userName)
        //{
        //    var result = new ResultModel<bool>();

        //    var isValid = Get(id);
        //    if (isValid == null || isValid.Data == null)
        //        throw new Exception($"Cost Center with ID {id} not exist");

        //    var exists = Get(id);
        //    if (exists == null || exists.Data == null)
        //        throw new Exception("reclass order {id} not found!");
        //    else
        //    {
        //        if (exists.Data.deleted ?? false)
        //            throw new Exception($"reclass order {id} already deleted!");
        //        else
        //        {
        //            var sqlUpdate = @"UPDATE cost_center SET 
        //                        deleted = @deleted,
        //                        update_by = @updateBy,
        //                        update_at = @updateAt
        //                        WHERE id = @id";

        //            var exec = DataService.Execute(sqlUpdate,
        //                       new
        //                       {
        //                           id = id,
        //                           deleted = true,
        //                           updateAt = DateTime.Now,
        //                           updateBy = userName
        //                       });
        //            if (exec <= 0)
        //                throw new Exception("Delete Data Failed");

        //            result.Data = true;
        //            result.Message = "OK";
        //        }
        //    }
        //    return result;
        //}
    }

}