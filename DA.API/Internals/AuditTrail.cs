﻿using DA.API.Models;
using DA.Models;
using DA.Utilities;

namespace DA.API.Internals
{
    public class AuditTrail
    {
        public static ResultPaging<List<AuditTrailViewModel>> GetAll(RequestPaging paging, string? method, string? action)
        {
            var res = new ResultPaging<List<AuditTrailViewModel>>();

            string qry = $@"select at.id, at.method, at.action, at.description, isnull(at.deleted,0) deleted, at.Created_By, at.Created_At
                            from audit_trail at
                            where 1=1 
                            and ISNULL(at.deleted,0) = 0
                            ";
            if (method != null)
            {
                qry += $" and at.method = {method} ";
            }
            if (action != null)
            {
                qry += $" and at.action = {action} ";
            }
            if (paging != null)
            {
                if (string.IsNullOrEmpty(paging.orderBy))
                    paging.orderBy = " Created_At desc ";

                res = DataService.FindListPaging<AuditTrailViewModel>(paging, qry);
            }
            else
            {
                qry += " order by do.Created_At desc ";
                res.Data = DataService.FindList<AuditTrailViewModel>(qry);
                res.TotalCount = res.Data.Count;
            }

            if (res.Data != null && res.Data.Count > 0)
            {
                res.Message = "OK";
            }
            else
            {
                res.Message = "Data Not Found!";
            }
            return res;
        }

        public static ResultModel<AuditTrailInputModel> Get(int id)
        {
            var res = new ResultModel<AuditTrailInputModel>();

            string qry = $@"select at.id, at.method, at.action, at.description, isnull(at.deleted,0) deleted, at.Created_By, at.Created_At
                            from audit_trail at
                            where 1=1 
                            and ISNULL(at.id,0) = @id
                            ";

            AuditTrailInputModel data = DataService.Find<AuditTrailInputModel>(qry, new { id });

            if (data != null)
            {
                res.Message = "OK";
            }
            else
            {
                res.Message = "Data Not Found!";
            }
            res.Data = data;
            return res;
        }

        public static ResultModel<object> Insert(string method, string action, string desc, string userName)
        {
            var res = new ResultModel<object>();
            var qry = $@"insert into audit_trail(method, action, description, created_by, created_at) values (@method, @action, @desc, @userName, getdate());
                        SELECT SCOPE_IDENTITY();";
            var exec = DataService.ExecuteScalar<int>(qry, new { method, action, desc, userName });
            if (exec <= 0)
                throw new Exception("Add Data Failed!");

            res.Data = new { id = exec };
            res.Message = "OK";

            return res;
        }
    }
}
