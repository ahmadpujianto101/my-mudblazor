using DA.Models;
using DA.Utilities;
using DA.Entities;

namespace DA.API.Internals
{
    public class Auth
    {
        public static LoginResultModel DoAuth(LoginInputModel data)
        {

            string sql = @$"SELECT usr.*, emp.id employee_id, emp.[status] employee_status, emp.department_id, la.name role
                            FROM UserMst usr
                            LEFT JOIN employee emp ON usr.UserName = emp.code 
                            INNER JOIN level_access la ON emp.level_access_id = la.id
                            WHERE usr.UserName = '{data.UserName}'"; //@UserName_

            var param = new { UserName_ = data.UserName };
            var user = DataService.Find<userInf>(sql, param);

            if (user == null)
                throw new Exception("Username Or Password is not correct.");

            string passwordEnc = data.Password.Encrypt();
            if (user.Password != passwordEnc)
                throw new Exception("Username Or Password is not correct.");

            if (!user.employee_status)
                throw new Exception("Data Employee is inactive ");

            var result = new LoginResultModel();
            result.UserInfo = user;
            result.UserInfo.department_id = user.department_id;

            return result;
        }

    }
}
