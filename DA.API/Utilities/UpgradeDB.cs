
namespace DA.Utilities
{

    public class UpgradeDB
    {
        public static void Execute()
        {
            try
            {
                var CheckDBExists = DataService.CheckDatabase();
                if (CheckDBExists)
                {
                    string sqlPath = $"{AppDomain.CurrentDomain.BaseDirectory}DB";
                    var sqlDirs = Directory.GetDirectories(sqlPath);
                    foreach (var dir in sqlDirs)
                    {
                        var files = Directory.GetFiles(dir, "*.sql");
                        foreach (var file in files)
                        {
                            try
                            {
                                Logger.WriteToConsole(Logger.Level.Info, $"Execute file {file}");
                                DataService.ExecuteNonQuery(File.ReadAllText(file));
                                string archPath = file.Replace("DB", "DB_Archive");

                                if (File.Exists(archPath))
                                    File.Delete(archPath);

                                var arcDir = Path.GetDirectoryName(archPath);
                                if (!Directory.Exists(arcDir))
                                    Directory.CreateDirectory(arcDir);

                                File.Move(file, archPath);

                            }
                            catch (Exception ex)
                            {
                                throw new Exception($"Failed execute file {file} : {ex.Message}");
                            }
                        }
                    }
                }
                else
                    throw new Exception("Database Is Not Exists or Failed to create to this server!");
            }
            catch (Exception ex)
            {
                Logger.WriteToConsole(Logger.Level.Error,ex.Message);
            }
        }
    }
}