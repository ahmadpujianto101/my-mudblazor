using System.Net.Mail;
using System.Reflection;

namespace DA.Utilities
{
    public static class Extension
    {
        public static string Base64Encode(this string plainText)
        {
            string result = "";
            try
            {
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
                result = System.Convert.ToBase64String(plainTextBytes);
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public static string Base64Decode(this string base64EncodedData)
        {

            string result = "";
            try
            {
                var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
                result = System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public static bool IsValidEmail(this string email)
        {
            if (string.IsNullOrEmpty(email)) return false;

            var valid = true;
            try
            {
                var emailAddress = new MailAddress(email);
            }
            catch
            {
                valid = false;
            }

            return valid;
        }


        #region EntitiesExtension

        #region Insert
        public static string GetInsertQuery<T>(this T data)
        {
            IEnumerable<PropertyInfo> Iprop = data.GetType().GetProperties();
            var fields = Iprop.Where(x => x.CustomAttributes.Where(c => c.AttributeType.Name.Equals("DatabaseGeneratedAttribute")).Count() == 0)
                              .Select(x => x.Name).ToList();
            return $@"insert into [{data.GetType().Name}]
                            ([{string.Join("],[", fields)}]) values 
                            ( @{string.Join(", @", fields)} )";            
        }
        public static int InsertData<T>(this List<T> data, bool IsTransaction = true)
        {
            if (data == null || data.Count == 0) return 0;

            string qry = GetInsertQuery(data);

            return DataService.Execute(qry, data, IsTransaction);
        }
        public static int InsertData<T>(this T data, bool IsTransaction = true)
        {
            if (data == null) return 0;

            string qry = GetInsertQuery(data);

            return DataService.Execute(qry, data, IsTransaction);
        }
        public static async Task<int> InsertDataAsync<T>(this List<T> data, bool IsTransaction = true)
        {
            if (data == null || data.Count == 0) return 0;

            string qry = GetInsertQuery(data[0]);

            return await DataService.ExecuteAsync(qry, data, IsTransaction);
        }
        public static async Task<int> InsertDataAsync<T>(this T data, bool IsTransaction = true)
        {
            if (data == null) return 0;

            string qry = GetInsertQuery(data);

            return await DataService.ExecuteAsync(qry, data, IsTransaction);
        }

        #endregion

        #region Update
        public static string GetUpdateQuery<T>(this T data)
        {
            IEnumerable<PropertyInfo> Iprop = data.GetType().GetProperties();
            var fields = Iprop.Where(x => !x.Name.ToLower().Contains("created_") &&
                                          x.CustomAttributes.Where(c => c.AttributeType.Name.Equals("DatabaseGeneratedAttribute")).Count() == 0 &&
                                          data.GetType().GetProperty(x.Name)?.GetValue(data) != null)
                              .Select(x => $"[{x.Name}] = @{x.Name} ").ToList();

            var Keyfields = Iprop.Where(x => x.CustomAttributes.Where(c => c.AttributeType.Name.Equals("KeyAttribute")).Count() != 0)
                                 .Select(x => $"and [{x.Name}] = @{x.Name}").ToList();

            return $@"update [{data.GetType().Name}] 
                            set {string.Join(", " + Environment.NewLine, fields)}
                            where 1=1 {string.Join(" ", Keyfields)} ";
        }
        public static int UpdateData<T>(this List<T> data, bool IsTransaction = true)
        {
            if (data == null || data.Count == 0) return 0;

            string qry = GetUpdateQuery(data[0]);
             
            return DataService.Execute(qry, data, IsTransaction);
        }
        public static int UpdateData<T>(this T data, bool IsTransaction = true)
        {
            if (data == null) return 0;

            string qry = GetUpdateQuery(data);

            return DataService.Execute(qry, data, IsTransaction);
        }
        public static async Task<int> UpdateDataAsync<T>(this List<T> data, bool IsTransaction = true)
        {
            if (data == null || data.Count == 0) return 0;

            string qry = GetUpdateQuery(data);
             
            return await DataService.ExecuteAsync(qry, data, IsTransaction);
        }
        public static async Task<int> UpdateDataAsync<T>(this T data, bool IsTransaction = true)
        {
            if (data == null) return 0;

            string qry = GetUpdateQuery(data);

            return await DataService.ExecuteAsync(qry, data, IsTransaction);
        }
        #endregion

        #region HardDelete
        public static string GetQueryHardDelete<T>(this T data)
        {
            IEnumerable<PropertyInfo> Iprop = data.GetType().GetProperties();
            var Keyfields = Iprop.Where(x => x.CustomAttributes.Where(c => c.AttributeType.Name.Equals("KeyAttribute")).Count() != 0)
                                 .Select(x => $"and [{x.Name}] = @{x.Name}").ToList();
            return $@"delete from [{data.GetType().Name}] 
                            where 1-1 {string.Join(" ", Keyfields)} ";

        }
        public static int HardDeleteData<T>(this List<T> data, bool IsTransaction = true)
        {
            if (data == null) return 0;

            string qry = GetQueryHardDelete(data[0]);

            return DataService.Execute(qry, data, IsTransaction);
        }
        public static int HardDeleteData<T>(this T data, bool IsTransaction = true)
        {
            if (data == null) return 0;

            string qry = GetQueryHardDelete(data);

            return DataService.Execute(qry, data, IsTransaction);
        }

        public static async Task<int> HardDeleteDataAsync<T>(this T data, bool IsTransaction = true)
        {
            if (data == null) return 0;

            string qry = GetQueryHardDelete(data);

            return await DataService.ExecuteAsync(qry, data, IsTransaction);
        }
        public static async Task<int> HardDeleteDataAsync<T>(this List<T> data, bool IsTransaction = true)
        {
            if (data == null) return 0;

            string qry = GetQueryHardDelete(data[0]);

            return await DataService.ExecuteAsync(qry, data, IsTransaction);
        }
        #endregion

        #region SoftDelete
        public static string GetQuerySoftDelete<T>(this T data)
        {
            IEnumerable<PropertyInfo> Iprop = data.GetType().GetProperties();
            
            var Delfields = Iprop.Where(x => x.Name.ToLower().Equals("deleted"))
                              .Select(x => $"[{x.Name}] = 1 ").ToList();

            if (Delfields.Count <= 0)
                throw new Exception($@"Table [{data.GetType().Name}] does not contain table deleted");
            
            var fields = Iprop.Where(x => (x.Name.ToLower().Equals("update_by") || x.Name.ToLower().Contains("update_at")) &&
                                          data.GetType().GetProperty(x.Name)?.GetValue(data) != null)
                              .Select(x => $"[{x.Name}] = @{x.Name} ").ToList();

            var Keyfields = Iprop.Where(x => x.CustomAttributes.Where(c => c.AttributeType.Name.Equals("KeyAttribute")).Count() != 0)
                                 .Select(x => $"and [{x.Name}] = @{x.Name}").ToList();

            fields.AddRange(Delfields);

            return $@"update [{data.GetType().Name}] 
                            set {string.Join(", " + Environment.NewLine, fields)}
                            where 1=1 {string.Join(" ", Keyfields)} ";
        }
        public static int SoftDeleteData<T>(this List<T> data, bool IsTransaction = true)
        {
            if (data == null) return 0;

            string qry = GetQuerySoftDelete(data[0]);

            return DataService.Execute(qry, data, IsTransaction);
        }
        public static int SoftDeleteData<T>(this T data, bool IsTransaction = true)
        {
            if (data == null) return 0;

            string qry = GetQuerySoftDelete(data);

            return DataService.Execute(qry, data, IsTransaction);
        }

        public static async Task<int> SoftDeleteDataAsync<T>(this T data, bool IsTransaction = true)
        {
            if (data == null) return 0;

            string qry = GetQuerySoftDelete(data);

            return await DataService.ExecuteAsync(qry, data, IsTransaction);
        }
        public static async Task<int> SoftDeleteDataAsync<T>(this List<T> data, bool IsTransaction = true)
        {
            if (data == null) return 0;

            string qry = GetQuerySoftDelete(data[0]);

            return await DataService.ExecuteAsync(qry, data, IsTransaction);
        }
        #endregion
        #endregion

    }
}