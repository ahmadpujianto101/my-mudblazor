using DA.Entities;
using DA.Models;
namespace DA.Utilities
{
    public static class FileManager
    {
        public static string Upload(string destination, IFormFile file)
        {
            var result = string.Empty;

            string uploadFolder = Path.Combine(destination, "Upload");
            if (!Directory.Exists(uploadFolder))
                Directory.CreateDirectory(uploadFolder);

            string filePath = Path.Combine(uploadFolder, file.FileName);

            using (var fileStream = new FileStream(filePath, FileMode.Create))
            {
                try
                {
                    file.CopyTo(fileStream);
                    result = filePath;
                }
                catch (Exception ex)
                {
                    throw new Exception("Upload failed when store in folder.");
                }
            }

            return result;
        }

        public static void Submit(string fileName, string pathFile, string proccessType, string userName)
        {
            var f = new FileProcessor();
            f.FileName = fileName;
            f.ProcessType = proccessType;
            f.FilePath = pathFile;
            f.Status = "PENDING";
            f.ImportedOn = DateTime.Now;
            f.ImportedBy = userName;
            f.ID = Guid.NewGuid();
            f.InsertData();
        }

        public static ResultPaging<List<FileProcessor>> FetchHistoryUpload(RequestPaging paging, string? mode = null)
        {
            var res = new ResultPaging<List<FileProcessor>>();
            var sql = "SELECT * FROM FileProcessor ";

            if (!string.IsNullOrEmpty(mode))
            {
                if (mode.ToLower() == "budget master")
                    sql += $" WHERE ProcessType = {FileType.BUDGET_MASTER}";
                else if (mode.ToLower() == "bcr")
                    sql += $" WHERE ProcessType like '%{FileType.BCR}%'";
            }

            if (paging != null)
            {
                if (string.IsNullOrEmpty(paging.orderBy))
                    paging.orderBy = " ImportedOn desc ";

                res = DataService.FindListPaging<FileProcessor>(paging, sql);
            }
            else
            {
                sql += " order by ImportedOn desc ";
                res.Data = DataService.FindList<FileProcessor>(sql);
                res.TotalCount = res.Data.Count;
            }

            if (res.Data != null && res.Data.Count > 0)
            {
                res.Message = "OK";
            }
            else
            {
                res.Message = "Data Not Found!";
            }
            return res;


        }
    }


}