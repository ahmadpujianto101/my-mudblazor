
using System.Security.Claims;
using System.Security.Principal;
namespace DA.Utilities
{
    public static class Claims
    {
        public static string UserName(this IPrincipal principal)
        {
            var claims = ((ClaimsPrincipal) principal).Claims.FirstOrDefault(x => x.Type == "UserName");
            var result = claims.Value;
            return result;
        }
    }
}