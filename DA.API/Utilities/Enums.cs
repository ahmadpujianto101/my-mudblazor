


namespace DA.Utilities
{
    public class Status
    {
        public const string InActive = "InActive";
        public const string Active = "Active";
    }

    public class EmailStatus
    {
        public const string Sent = "SENT";
        public const string Failed = "FAILED";
    }

    public class TransactionType
    {
        public const string reclass_order = "reclass_order";
        public const string bcr_final = "bcr_final";
        public const string create_purchase_request = "create_purchase_request";
        public const string approve_purchase_request = "approve_purchase_request";
        public const string verification_purchase_request = "verification_purchase_request";
        public const string sendback_purchase_request = "sendback_purchase_request";
        public const string review_purchase_request = "review_purchase_request";
        public const string create_purchase_order = "create_purchase_order";
        public const string approve_purchase_order = "approve_purchase_order";
        public const string verification_purchase_order = "verification_purchase_order";
        public const string sendback_purchase_order = "sendback_purchase_order";
        public const string review_purchase_order = "review_purchase_order";
        public const string reject_purchase_request = "reject_purchase_request";


    }

    public class FileType 
    {
        public const string BUDGET_MASTER = "BUDGET MASTER";
        public const string BCR = "BCR_";
    }

    public class BudgetScan
    {
        public const string Yes = "Yes";
        public const string No = "No";
    }
}