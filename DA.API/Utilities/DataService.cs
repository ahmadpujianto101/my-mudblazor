using System.Data.SqlClient;
using ConfigurationManager = System.Configuration.ConfigurationManager;
using Dapper;
using DA.Models;
using Newtonsoft.Json;

namespace DA.Utilities
{
    public class DataService
    {
        private static string _connectionString = string.Empty;

        public static string ConnectionString
        {
            get
            {
                return _connectionString;
            }
            set
            {
                _connectionString = value;
            }
        }

        private static void Init()
        {
            string provider = ConfigurationManager.AppSettings["DefaultProvider"] + "";
            if (string.IsNullOrEmpty(ConnectionString))
                ConnectionString = ConfigurationManager.ConnectionStrings[provider].ConnectionString;
        }

        public static int ExecuteNonQuery(SqlCommand cmd)
        {
            if (string.IsNullOrEmpty(ConnectionString))
                Init();

            int result = 0;
            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                conn.Open();
                cmd.Connection = conn;
                result = cmd.ExecuteNonQuery();
            }
            return result;
        }

        public static int ExecuteNonQuery(string query, params SqlParameter[] parameters)
        {
            if (string.IsNullOrEmpty(ConnectionString))
                Init();

            int result = 0;

            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                conn.Open();
                string commandtext = query;

                SqlCommand cmd = new SqlCommand(commandtext, conn);
                if (parameters != null)
                {
                    foreach (var p in parameters)
                        cmd.Parameters.Add(p);
                }

                result = cmd.ExecuteNonQuery();
            }

            return result;
        }
        public static bool CheckConnection()
        {
            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                conn.Open();
                return true;
            }
        }

        public static async Task<bool> CheckConnectionAsync()
        {
            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                await conn.OpenAsync();
                return true;
            }
        }

        public static bool CheckDatabase()
        {
            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                var CurDatabase = conn.Database;
                conn.ConnectionString = ConnectionString.Replace(CurDatabase, "master");
                conn.Open();

                var qry = $"SELECT db_id('{CurDatabase}')";

                int? exists = conn.ExecuteScalar<int>(qry);
                if (exists == null || exists <= 0)
                {
                    qry = $"Create database {CurDatabase}";
                    conn.Execute(qry);
                }

                conn.ChangeDatabase(CurDatabase);

                return true;
            }
        }

        public static async Task<bool> CheckDatabaseAsync()
        {
            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                var CurDatabase = conn.Database;
                conn.ConnectionString = ConnectionString.Replace(CurDatabase, "master");
                await conn.OpenAsync();
                var qry = $"SELECT db_id('{CurDatabase}')";

                int? exists = await conn.ExecuteScalarAsync<int>(qry);
                if (exists == null || exists <= 0)
                {
                    qry = $"Create database {CurDatabase}";
                    conn.Execute(qry);
                }

                conn.ChangeDatabase(CurDatabase);
                return true;
            }
        }

        public static int Execute(string cmd, object? parameter = null, bool IsTransaction = false)
        {
            var res = 0;
            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                conn.Open();
                SqlTransaction? trans = null;
                try
                {
                    if (IsTransaction)
                        trans = conn.BeginTransaction();

                    res = conn.Execute(cmd, parameter, trans);

                    if (IsTransaction && trans != null)
                        trans.Commit();
                }
                catch (Exception ex)
                {
                    if (IsTransaction && trans != null) 
                        trans.Rollback();
                    trans = null;
                    res = 0;
                    throw;
                }
            }
            return res;
        }
        public static async Task<int> ExecuteAsync(string cmd, object? parameter = null, bool IsTransaction = false)
        {
            var res = 0;
            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                await conn.OpenAsync();
                SqlTransaction? trans = null;
                try
                {
                    if (IsTransaction)
                        trans = conn.BeginTransaction();

                    res = await conn.ExecuteAsync(cmd, parameter, trans);

                    if (IsTransaction && trans != null)
                        trans.Commit();
                }
                catch
                {
                    if (IsTransaction && trans != null)
                        trans.Rollback();
                    trans = null;
                    res = 0;
                    throw;
                }
            }
            return res;
        }
        
        public static T ExecuteScalar<T>(string cmd, object? parameter = null)
        {
            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                conn.Open();
                return conn.ExecuteScalar<T>(cmd, parameter);
            }
        }
        public static async Task<T> ExecuteScalarAsync<T>(string cmd, object? parameter = null)
        {
            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                await conn.OpenAsync();
                return await conn.ExecuteScalarAsync<T>(cmd, parameter);
            }
        }

        public static ResultPaging<List<T>> FindListPaging<T>(RequestPaging paging, string cmd, object? parameter = null)
        {
            var res = new ResultPaging<List<T>>() { CurrentPage = paging.CurrentPage, ShowPerPage = paging.ShowPerPage };
            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                conn.Open();
                String cmdCount = $" select count(1) Total from ({cmd}) Data";
                int count = (int)conn.ExecuteScalar(cmdCount, parameter);
                if (count == 0)
                {
                    res.Message = "Data Not Found";
                    res.CurrentPage = 1;
                    res.TotalPage = 1;
                    res.TotalCount = count;
                    res.ShowPerPage = paging.ShowPerPage;
                }
                else
                {
                    res.Message = "OK";
                    res.TotalPage = (int)Math.Ceiling((double)count / (double)paging.ShowPerPage);
                    res.CurrentPage = res.TotalPage >= paging.CurrentPage ? paging.CurrentPage : 1;
                    res.TotalCount = count;
                    res.ShowPerPage = paging.ShowPerPage;
                }

                string cmdPaging = $" Select * from ({cmd}) data " +
                                   $" order by {(paging.orderBy ?? "(select 1)")} " +
                                   $" OFFSET {res.Offset()} ROWS " +
                                   $" FETCH NEXT {paging.ShowPerPage} ROWS ONLY ";
                res.Data = conn.Query<T>(cmdPaging, parameter).ToList();
                return res;
            }
        }
        public static async Task<ResultPaging<List<T>>> FindListPagingAsync<T>(RequestPaging paging, string cmd, object? parameter = null)
        {
            var res = new ResultPaging<List<T>>() { CurrentPage = paging.CurrentPage, ShowPerPage = paging.ShowPerPage };
            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                await conn.OpenAsync();
                String cmdCount = $" select count(1) Total from ({cmd}) Data";
                int count = await conn.ExecuteScalarAsync<int>(cmdCount, parameter);
                if (count == 0)
                {
                    res.Message = "Data Not Found";
                    res.CurrentPage = 1;
                    res.TotalPage = 1;
                    res.TotalCount = count;
                    res.ShowPerPage = paging.ShowPerPage;
                }
                else
                {
                    res.Message = "OK";
                    res.TotalPage = (int)Math.Ceiling((double)count / (double)paging.ShowPerPage);
                    res.CurrentPage = res.TotalPage >= paging.CurrentPage ? paging.CurrentPage : 1;
                    res.TotalCount = count;
                    res.ShowPerPage = paging.ShowPerPage;
                }

                string cmdPaging = $" Select * from ({cmd}) data " +
                                   $" order by {(paging.orderBy ?? "(select 1)")} " +
                                   $" OFFSET {res.Offset()} ROWS " +
                                   $" FETCH NEXT {paging.ShowPerPage} ROWS ONLY ";
                var Exec = await conn.QueryAsync<T>(cmdPaging, parameter);
                res.Data = Exec.ToList();
                return res;
            }
        }

        public static List<T> FindList<T>(string cmd, object? parameter = null)
        {

            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                conn.Open();
                return conn.Query<T>(cmd, parameter).ToList();
            }

        }
        public static async Task<List<T>> FindListAsync<T>(string cmd, object? parameter = null)
        {

            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                await conn.OpenAsync();
                var res = await conn.QueryAsync<T>(cmd, parameter);
                return res.ToList();
            }

        }

        public static T Find<T>(string cmd, object? param = null)
        {
            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                conn.Open();
                return conn.QuerySingleOrDefault<T>(cmd, param);
            }
        }
        public static async Task<T> FindAsync<T>(string cmd, object? param = null)
        {
            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                await conn.OpenAsync();
                return await conn.QuerySingleOrDefaultAsync<T>(cmd, param);
            }
        }

        public static int ExecuteMultiple(List<DapperTransaction> data)
        {
            var queryIndex = 0;
            var res = 0;
            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                conn.Open();
                using (SqlTransaction trans = conn.BeginTransaction())
                {
                    try
                    {
                        foreach (var x in data)
                        {
                            res += conn.Execute(x.Query, x.QueryParameter, trans);
                            queryIndex ++;
                        }
                        trans.Commit();
                    }
                    catch (Exception ex)
                    {
                        trans.Rollback();
                        res = 0;
                        throw new Exception($"{ex.Message} | QueryIx {queryIndex}: {JsonConvert.SerializeObject(data[queryIndex])}");
                    }
                }
            }
            return res;
        }
        public static async Task<int> ExecuteMultipleAsync(List<DapperTransaction> data)
        {
            var queryIndex = 0;
            var res = 0;
            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                await conn.OpenAsync();
                using (SqlTransaction trans = conn.BeginTransaction())
                {
                    try
                    {
                        foreach (var x in data)
                        {
                            res += await conn.ExecuteAsync(x.Query, x.QueryParameter, trans);
                            queryIndex++;
                        }
                        trans.Commit();
                    }
                    catch (Exception ex)
                    {
                        trans.Rollback();
                        res = 0;
                        throw new Exception($"{ex.Message} | QueryIx {queryIndex}: {JsonConvert.SerializeObject(data[queryIndex])}");
                    }
                }
            }
            return res;
        }

    }


    public class DapperTransaction
    {
        public string Query { get; set; }
        public Object QueryParameter { get; set; }
    }
}