namespace DA.Utilities
{
    public static class Logger
    {
        public enum Level
        {
            Info,
            Error,
        }
        public static void WriteToConsole(Level level, string message)
        {
            Console.Write($"{DateTime.Now} : ");

            if (level == Level.Error)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write($"::FATAL ERROR:: ");
            }
            else if (level == Level.Info)
            {
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.Write($"::INFO:: ");
            }

            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine($"{message}");
        }
    }

}