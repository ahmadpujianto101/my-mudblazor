using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using DA.Middleware;
using Microsoft.OpenApi.Models;
using Microsoft.AspNetCore.Server.HttpSys;

var builder = WebApplication.CreateBuilder(args);

builder.Logging.ClearProviders();
builder.Logging.AddConsole(opt => {
    opt.UseUtcTimestamp = true;
    opt.TimestampFormat = "yyyy-MM-dd HH:mm:ss ";
});



string connectionString = string.Empty;
connectionString = builder.Configuration.GetConnectionString("SunLife");
DA.Utilities.DataService.ConnectionString = connectionString;
DA.Utilities.UpgradeDB.Execute();

var MailConfig = builder.Configuration.GetSection("Email");
//if (MailConfig != null && MailConfig.GetChildren().Count() > 0)
//{
//    DA.Utilities.EmailService.sender = new DA.API.Models.EmailModel()
//    {
//        emailServer = MailConfig.GetValue<string>("Server") ?? "",
//        emailPort = MailConfig.GetValue<int>("Port"),
//        emailUsername = MailConfig.GetValue<string>("Username") ?? "",
//        emailPassword = MailConfig.GetValue<string>("Password") ?? "",
//        emailDisplay = MailConfig.GetValue<string>("emailDisplay") ?? "Sunlife",
//        UseDefaultCredentials = MailConfig.GetValue<bool>("UseDefaultCredentials"),
//    };
//}

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();


#region using JWT auth
builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v1", new OpenApiInfo
    {
        Title = "Sunfile_API",
        Version = "v1"
    });
    c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
    {
        Name = "Authorization",
        Type = SecuritySchemeType.ApiKey,
        BearerFormat = "JWT",
        In = ParameterLocation.Header,
        Description = "Here Enter JWT Token with bearer format like bearer[space] token"
    });
    c.AddSecurityRequirement(new OpenApiSecurityRequirement
    {
        {
            new OpenApiSecurityScheme
            {
                Reference = new OpenApiReference
                {
                    Type = ReferenceType.SecurityScheme,
                    Id = "Bearer"
                }
            },
            new string[] { }
        }
    });
});

builder.Services.AddAuthentication(option =>
{
    option.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
    option.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;

}).AddJwtBearer(options =>
{
    options.TokenValidationParameters = new TokenValidationParameters
    {
        ValidateIssuer = true,
        ValidateAudience = true,
        ValidateLifetime = true,
        ValidateIssuerSigningKey = true,
        ValidIssuer = builder.Configuration["Jwt:Issuer"],
        ValidAudience = builder.Configuration["Jwt:Audience"],
        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(builder.Configuration["Jwt:Key"])) //Configuration["JwtToken:SecretKey"]
    };
});
var allOrigins = "allowOrigins";
builder.Services.AddCors(opt => opt.AddPolicy(allOrigins,
                                            policy =>
                                            {
                                                policy.WithOrigins("*").AllowAnyMethod().AllowAnyHeader();
                                            }));
#endregion using JWT auth

#region using Windows Auth
//builder.Services.AddSwaggerGen(c =>
//{
//    c.SwaggerDoc("v1", new OpenApiInfo
//    {
//        Title = "Sunfile_API",
//        Version = "v1"
//    });

//    c.AddSecurityDefinition("WindowsAuth", new OpenApiSecurityScheme
//    {
//        Name = "Authorization",
//        Type = SecuritySchemeType.Http,
//        Scheme = "Negotiate", // Specify Negotiate as the scheme
//        In = ParameterLocation.Header,
//        Description = "Here Enter Negotiate Token"
//    });

//    c.AddSecurityRequirement(new OpenApiSecurityRequirement
//    {
//        {
//            new OpenApiSecurityScheme
//            {
//                Reference = new OpenApiReference
//                {
//                    Type = ReferenceType.SecurityScheme,
//                    Id = "WindowsAuth"
//                }
//            },
//            new string[] { }
//        }
//    });
//});

//builder.Services.AddAuthentication(HttpSysDefaults.AuthenticationScheme);

//builder.Services.AddCors(opt => opt.AddPolicy("allowOrigins",
//    policy => policy.WithOrigins("*").AllowAnyMethod().AllowAnyHeader()));
#endregion using Windows Auth

builder.Services.AddHttpClient();
var app = builder.Build();



// Configure the HTTP request pipeline.
// if (app.Environment.IsDevelopment())
// {
app.UseSwagger();
app.UseSwaggerUI();
// }

app.UseHttpLogging();

app.UseHttpsRedirection();

app.UseCors(allOrigins);

// app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.UseMiddleware<LoggingMiddleware>();

app.Run();
