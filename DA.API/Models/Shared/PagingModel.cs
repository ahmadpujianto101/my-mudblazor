﻿
namespace DA.Models
{
    public class PagingModel
    {
        public int? CurrentPage { get; set; } = 0;
        public int? ShowPerPage { get; set; } = 0;
        public int ? Offset() { return (CurrentPage - 1) * ShowPerPage; }
    }
    public class RequestPaging : PagingModel
    {
        public string? orderBy { get; set; }
    }

    public class ResultPaging<T> : PagingModel
    {
        public string Message {get; set;} = string.Empty;
        public T? Data { get; set; }
        public int? TotalPage { get; set; } = 0;
        public int TotalCount { get; set; } = 0;
    }
}
