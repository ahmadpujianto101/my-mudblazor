namespace DA.Models
{
    public class BaseViewModel
    {
        public string Created_By {get; set;}
        public DateTime Created_At {get; set;}
        public string Update_By {get; set;}
        public DateTime Update_At {get; set;}
    }
}