namespace DA.Models
{
    public class LookupModel
    {
        public dynamic? id { get; set; }
        public string? name { get; set; }
    }
}