using DA.Entities;
namespace DA.Models
{
    public class LoginInputModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }

    public class LoginResultModel
    {
        public userInf UserInfo { get; set; }
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
    }

    public class userInf
    {
        public string UserName { get; set; }


        public string DisplayName { get; set; }


        public string Password { get; set; }


        public string Remark { get; set; }


        public int? employee_id { get; set; }
        public string department_id { get; set; }
        public bool employee_status { get; set; }

        public string role { get; set; }


        public bool? Deleted { get; set; }


        public string Created_By { get; set; }


        public DateTime? Created_At { get; set; }


        public string Update_By { get; set; }


        public DateTime? Update_At { get; set; }
        public string UserLevel { get; set; }

    }
}