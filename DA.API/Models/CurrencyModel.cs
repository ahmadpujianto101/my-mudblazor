﻿namespace DA.API.Models
{
    public class CurrencyViewModel
    {
        public int id { get; set; }

        public string? name { get; set; }

        public int? currency_to_idr { get; set; }

        public bool? deleted { get; set; }

        public string? Created_By { get; set; }

        public DateTime? Created_At { get; set; }

        public string? Update_By { get; set; }

        public DateTime? Update_At { get; set; }

    }
    public class CurrencyInputModel
    {
        public int id { get; set; }

        public string? name { get; set; }

        public int? currency_to_idr { get; set; }

        public bool? deleted { get; set; }

    }
}
